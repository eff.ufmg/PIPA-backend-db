# test_ranger.py
from flask import jsonify
from apache_ranger.model.ranger_service import *
from apache_ranger.client.ranger_client import *
from apache_ranger.model.ranger_policy  import *

class Ranger:

    def __init__ (self, RANGER_DOMAIN, RANGER_ROOT_USERNAME, RANGER_ROOT_PASSWORD):
        self.RANGER_DOMAIN = RANGER_DOMAIN
        self.RANGER_ROOT_USERNAME = RANGER_ROOT_USERNAME
        self.RANGER_ROOT_PASSWORD = RANGER_ROOT_PASSWORD


    def rangerAuth(self):

        ranger_url = self.RANGER_DOMAIN
        ranger_auth = (self.RANGER_ROOT_USERNAME, self.RANGER_ROOT_PASSWORD)
        ranger = RangerClient(ranger_url, ranger_auth)

        return ranger

    def getRangerPolicies(self):
            
            try:
                ranger = self.rangerAuth()
                policies = ranger.find_policies()

                dictOfRangerPolicies = {}

                for policy in policies:
                        chave = policy["name"]
                        objeto = {
                             "id" : policy["id"],
                             "name" : policy["name"]
                        }
                        dictOfRangerPolicies[chave] = objeto
                
                return dictOfRangerPolicies

            except (Exception) as erro:
                return "Failed to get policies in Ranger " + str(erro)
            
    def retrievePolicyRanger(self, policyid):

        try:
            ranger = self.rangerAuth()
            retrieved_policy = ranger.get_policy_by_id(policyid)
            return retrieved_policy

        except Exception as erro:
            return "Failed to retrieve policy in Ranger: " + str(erro)

    
    def putUserInPolicyRanger(self, policy, dictOfUsers):

        try:
            ranger = self.rangerAuth()
            policy.policyItems[0]["users"] = dictOfUsers
            ranger.update_policy_by_id(policy.id, policy)

        except Exception as erro:
            return "Failed to put user in a policy in Ranger: " + str(erro)

